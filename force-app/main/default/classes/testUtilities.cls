@IsTest
global class testUtilities {

    @TestSetup
    static void testSetup(){
        List<Account> accList = new List<Account>();
        for(Integer i = 0 ; i < 100 ; i++){
            Account temp = new Account(Name = 'Test Acc ' + i);
            accList.add(temp);
        }
        insert accList;

        List<Contact> contactList = new List<Contact>();
        for(Integer i = 0 ; i < accList.size() ; i++){
            Contact temp = new Contact(MailingStreet = '8504 Heather Hill Drive',MailingCity = 'Springfield',MailingState = 'Virginia',MailingPostalCode = '22154',MailingCountry = 'United States',LastName = 'Test Contact ' + i, AccountId = accList[i].Id);
            contactList.add(temp);
        }
        insert contactList;

        //create partner users
        List<User> userList = new List<User>();
        Id p = [select id from profile where name='Partner Community User'].id;
        for(Integer i = 0 ; i < contactList.size() ; i++){
            User user = new User(alias = 'test' + i, 
                    email='tester' + i + '@yopmail.com',
                    emailencodingkey ='UTF-8', 
                    lastname='Testing', 
                    languagelocalekey='en_US',
                    localesidkey='en_US', 
                    profileid = p, 
                    IsActive = true,
                    timezonesidkey = 'America/Los_Angeles',
                    ContactId = contactList[i].Id,
                    Street = contactList[i].MailingStreet,
                    City = contactList[i].MailingCity,
                    State = contactList[i].MailingState,
                    PostalCode = contactList[i].MailingPostalCode,
                    Country = contactList[i].MailingCountry,
                    username='tester' + i + '@yopmail.com');
        
           userList.add(user);
        }
        insert userList;
        
    }
    @isTest
    Public static void TestMethod1(){
        List<User> users = [SELECT Id, Street,City,State,PostalCode,Country FROM User WHERE LastName  = 'Testing'];
        for(User u: users){
            u.Street = '8505 Heather Point Avenue';
            
        }
        Test.startTest();
        update users;
        Test.stopTest();
        // Validate the expected results.
        List<Contact> contacts = [SELECT Id,MailingStreet FROM Contact WHERE LastName  LIKE 'Test Contact'];
        for(Contact c: contacts){
            System.assertEquals(c.MailingStreet, '8505 Heather Point Avenue');
        }
    
    }

    

}