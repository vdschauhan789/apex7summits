public with sharing class UserTriggerHandler{
   
    public void OnBeforeInsert(List<User> newUsers)
    {
        system.debug('User Trigger On Before Insert');
    }
    public void OnAfterInsert(List<User> newUsers)
    {
        system.debug('User Trigger On After Insert');
    }
    public void OnAfterUpdate( List<User> newUsers, List<User> oldUsers, Map<ID, User> newUserMap , Map<ID, User> oldUserMap )
    {
        system.debug('User Trigger On After Update');
        updateContactsOnAddressChange(newUserMap , oldUserMap);
    }
    public void OnBeforeUpdate( List<User> newUsers, List<User> oldUsers, Map<ID, User> newUserMap , Map<ID, User> oldUserMap )
    {
        system.debug('User Trigger On Before Update');
    }

    private void updateContactsOnAddressChange(Map<Id,User> newUserMap, Map<Id,User> oldUserMap){
        
        Map<Id,User> contactIdUserMap = new Map<Id,User>();
        for( Id userId : newUserMap.keySet() )
        {
          if(newUserMap.get(userId).ContactId != null && (oldUserMap.get(userId).Street != newUserMap.get(userId).Street || 
                oldUserMap.get(userId).City != newUserMap.get(userId).City
              || oldUserMap.get(userId).State != newUserMap.get(userId).State 
              || oldUserMap.get(userId).PostalCode != newUserMap.get(userId).PostalCode 
              || oldUserMap.get(userId).Country != newUserMap.get(userId).Country))
          {
            contactIdUserMap.put(newUserMap.get(userId).ContactId, newUserMap.get(userId));
          }
        }
        system.debug('contactIdUserMap ' + contactIdUserMap);
        if(contactIdUserMap.keySet().size()> 0){
            List<String> contacts = new List<String>();
            Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT Id,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry FROM Contact WHERE Id IN: contactIdUserMap.keySet()]);
            for(Id contactId : contactMap.keySet()){
                if(contactIdUserMap.get(contactId).Street != null){
                    contactMap.get(contactId).MailingStreet = contactIdUserMap.get(contactId).Street;
                }
                if(contactIdUserMap.get(contactId).City != null){
                    contactMap.get(contactId).MailingCity = contactIdUserMap.get(contactId).City;
                }
                if(contactIdUserMap.get(contactId).State != null){
                    contactMap.get(contactId).MailingState = contactIdUserMap.get(contactId).State;
                }
                if(contactIdUserMap.get(contactId).PostalCode != null){
                    contactMap.get(contactId).MailingPostalCode = contactIdUserMap.get(contactId).PostalCode;
                }
                if(contactIdUserMap.get(contactId).Country != null){
                    contactMap.get(contactId).MailingCountry = contactIdUserMap.get(contactId).Country;
                }
                contacts.add(JSON.serialize(contactMap.get(contactId)));
               
            }
            system.debug('update**'+ contactMap.values() );
            updateContactFuture(contacts);
        }


        
    }
    @Future
    private static void updateContactFuture(List<String> contacts){
        List<Contact> updateContactList = new List<Contact>();
        for(String cont : contacts){
            Contact con = (Contact)JSON.deserialize(cont, Contact.class);
            updateContactList.add(con);
        }
        update updateContactList;
    
    }
}
