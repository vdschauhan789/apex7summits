import { LightningElement } from 'lwc';
import * as RESUME_DATA from './myResumeContainerData'
export default class MyResumeContainer extends LightningElement {
    //https://www.salesforcetroop.com/build-and-publish-your-resume-using-lwc
    PROFILE_IMAGE = RESUME_DATA.PROFILE_IMAGE
    SOCIAL_LINKS = RESUME_DATA.SOCIAL_LINKS
    RESUME_DATA = RESUME_DATA
}