import SOCIAL from '@salesforce/resourceUrl/SOCIAL'
import MyPicture from '@salesforce/resourceUrl/MyPicture'
export const PROFILE_IMAGE = MyPicture
export const SOCIAL_LINKS = [
    {
        type: "linkedin",
        label: "linkedin/vdsc",
        link: "https://www.linkedin.com/in/vanshdeepchauhan/",
        icon: SOCIAL + '/SOCIAL/linkedin.svg'
    }

]

export const USER_DETAILS = {
    NAME: 'Vanshdeep Chauhan',
    ROLE: 'Salesforce Developer',
    EMAIL: 'vdschauhan789@gmail.com',
    PHONE: '3023453474'
}

export const CAREER_SUMMARY={
    HEADING:"SUMMARY",
    DESCRIPTION: "Results oriented, Customer savvy software engineering leader/consultant experienced engaging sales organizations and customer to drive develop and implement custom software solutions, looking for career growth",
    /*KEYS_POINTS:[
        "If You want to add summary points it comes here. and its optional",
        "keypoint 2 Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
        "keypoint 3 Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        "keypoint 4 Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
    ]*/
}

export const EXPERIENCE_DATA={
    HEADING: "Work Experience",
    EXPERIENCES: [
        {
            ROLE: "Senior Software Engineer",
            COMPANY_NAME: "REI Systems Inc",
            DURATION: "2021 - Present",
            DESCRIPTION:
                 "Your Job description goes hereiusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. ",
            DESCRIPTION_POINTS: [
                "Responsible for developing in Apex code, Visualforce pages, and other technologies to build customized solutions that support business requirements and drive key business decisions",
                "Performed technical analysis, level-of-effort estimation, design, development, implementation of applications with necessary customizations and integration",
                "Performed Mass Data Migrations using different data migration tools such as data loader, dataloader.io",
                "Developed proof of concepts for various complex requirements and worked with the functional product owners, technical leads, and project managers to demonstrate and finalize a solution",
                "Supported integration of external systems with Salesforce.com using web services",
                "Created data reports using standard reporting capability in Salesforce. Also, develop custom reports that require complex data join",
                "Performed deployments using ClickDeploy and Github",
                "Participated in sales presentations due to ability to translate user needs into easy-to-understand software solutions. Helped sales team close two major deals generating more than $300K in revenue",
                "Participated in agile implementation efforts for the team by productively engaging in events such as Sprint Planning, Daily Sprints and Sprint Reviews and analyze project activities for continuous improvements",
                "Produced project documentation, technical designs, and end-user guides"

            ],
            TECHNOLOGIES_USED: {
                HEADING: 'Technologies used',
                LIST: [
                    "Apex",
                    "SOQL",
                    "Salesforce",
                    "HTML",
                    "Javascript",
                    
                ]
            },
        },
        {
            ROLE: "Software Engineer",
            COMPANY_NAME: "REI Systems Inc",
            DURATION: "2017 - 2020",
            DESCRIPTION:
                "Your Job description goes here iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. ",
            DESCRIPTION_POINTS: [
                "Customized REI Systems' GovGrants product to accommodate the grants management business processes based on client's requirements",
                "Identifying bottlenecks and redundancies in apex controllers and triggers that raised system efficiency by 23%",
                "Worked with business team and the development team to document and develop enhancements to the software application",
                "Developed Excel upload functionality which fueled revenue stream through additional user base, generating about $30K in new license sales within first few weeks of project release",
                "Performed troubleshooting and fixed defects for planned releases and production issues",
                "Implemented new enhancements including creation of custom objects, workflows, email alerts, templates and UI changes"

            ],
            TECHNOLOGIES_USED: {
                HEADING: 'Technologies used',
                LIST: [
                    "Apex",
                    "SOQL",
                    "Salesforce",
                    "HTML",
                    "Javascript",
                    
                ]
            }
        },
        
    ]
}


 export const EDUCATION_DATA={
     ICON: SOCIAL + '/SOCIAL/education.svg',
     HEADING: "EDUCATION",
     LIST:[
         {
             NAME: "Master of Science Computer Engineering",
             UNIVERSITY_NAME: "University of Delaware, USA",
             DURATION: "2015 - 2017",
         },
         {
             NAME: "Bachelors of Tech. Electronics Engineering",
             UNIVERSITY_NAME: "Vellore Institute of Technology, India",
             DURATION: "2009 - 2013",
         }
     ]
 }

 export const AWARDS_DATA={
     ICON: SOCIAL + '/SOCIAL/awards.svg',
     HEADING: "AWARDS",
     LIST: [
         {
             NAME: "Going the Extra Mile Award 2021, REI Systems Inc.",
             //DESCRIPTION:"Award description goes here, sem. Nulla consequat massa quis enim. Donec pede justo.",
         },
         {
             NAME: "Going the Extra Mile Award 2020, REI Systems Inc.",
             //DESCRIPTION:"Award description goes here, sem. Nulla consequat massa quis enim. Donec pede justo.",
         },
     ]
 }

export const CERTIFICATION_DATA={
    ICON: SOCIAL + '/SOCIAL/certification.svg',
    HEADING: "CERTIFICATIONS",
    LIST: [
        {
            NAME: "Salesforce Platform Developer 1",
        },
        
    ]
}

export const LANGUAGES_DATA={
    HEADING: "Languages",
    LIST: [
        {
            NAME: "English",
            //LEVEL: "Professional",
        },
        {
            NAME: "Hindi",
            //LEVEL: "Professional",
        },
        
    ]
}

export const SKILLS_DATA ={
    HEADING: "SKILLS & TOOLS",
    SKILLS:[
        
        {
            HEADING: "Programming Languages",
            SKILLS_LIST: [
                { NAME: "Apex", LEVEL: "80" },
                { NAME: "Visualforce", LEVEL: "95" },
                { NAME: "SOQL", LEVEL: "98" },
                { NAME: "HTML", LEVEL: "98" },
                { NAME: "CSS", LEVEL: "98" },
                { NAME: "Javascript", LEVEL: "98" },
                { NAME: "Java", LEVEL: "98" },
                { NAME: "Web Services", LEVEL: "98" },
            ],
        },
        {
            HEADING: "CRM/CMS",
            SKILLS_LIST: [
                { NAME: "Salesforce", LEVEL: "50" },
                
            ],
        }
    ],
    OTHERS_SKILLS:{
        HEADING: 'OTHERS',
        SKILLS_LIST: [
            "Git",
            "IntelliJ",
            "Visual Studio Code",
            "Click Deploy",
            "Data Loader",
            "MS Visio",
            "MS Excel",
            ]
    }
}

export const INTERESTS_DATA = {
    HEADING: "Interests",
    LIST: ["Reading", "Cooking", "Soccer"]
}